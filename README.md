# Rebrain DevOps docs

Repository of completed tasks on the rebrain course.

## Setup initial environment

Before you begin, you must install git.

For Ubuntu 18.04:

`$ sudo apt update -y && apt install -y git`

For CentOS 7:

`$ sudo yum update -y && yum install -y git`

To clone a project from a remote repository:

    $ cd ~/
    $ git clone https://github.com/dan4ex/rebrain-devops-task-checkout.git
    $ cd ./rebrain-devops-task-checkout

## Beginning of work

After that, we can perform tasks and work with this project.

When you have completed the tasks and made some changes, you need to index the current state of the project:

`$ git add .`

And commit the changes:

`$ git commit`

## Finish of work

Having finished working with the project, push in to the remote repository:

`$ git push origin`

## Authors

* Daniil Osipov - *student of rebrain* - [Join to my GitLab](https://gitlab.rebrainme.com/dan4ex).

## License

This project is licensed under the Rebrain License - see the LICENSE.md file for details.